import { Component, EventEmitter, Output } from "@angular/core";
import { DataStorageService } from "../../shared/data-storage.service";
import { Response } from "@angular/http";
import { AuthService } from "../../auth/auth.service";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    //     @Output() featureSelected = new EventEmitter<string>();
    //     onSelect(feature: string) {
    //         this.featureSelected.emit(feature);
    //     }

    constructor(private datStorageService: DataStorageService,
        public authService: AuthService) { }
    onSaveData() {
        this.datStorageService.storeRecipes()
            .subscribe(
            (response: Response) => {
                console.log(response);
            });
    }
    onFetchData() {
        this.datStorageService.fetchRecipes();
    }
    onLogout() {
        this.authService.logout();
    }
}