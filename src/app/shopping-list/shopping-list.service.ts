import { Ingredient } from "../shared/ingredient.model";
import { EventEmitter } from "@angular/core";
import { Subject } from "rxjs/Subject";

export class ShoppingListService {

    startEditing = new Subject<number>();

    // ingredientChanged = new EventEmitter<Ingredient[]>();
    ingredientChanged = new Subject<Ingredient[]>();
    private ingredients: Ingredient[] = [
        new Ingredient('Apples', 5),
        new Ingredient('Tomato', 7)
    ];
    getIngredients() {
        return this.ingredients.slice();
    }
    getIngrefient(index: number) {
        return this.ingredients[index];
    }
    updateIngredient(index: number, newIngredient: Ingredient) {
        this.ingredients[index] = newIngredient;
        this.ingredientChanged.next(this.ingredients.slice());
    }
    addIngredient(ingredient: Ingredient) {
        this.ingredients.push(ingredient);
        // this.ingredientChanged.emit(this.ingredients.slice())
        this.ingredientChanged.next(this.ingredients.slice())
    }
    addIngredients(ingredients: Ingredient[]) {
        this.ingredients.push(...ingredients);
        // this.ingredientChanged.emit(this.ingredients.slice())
        this.ingredientChanged.next(this.ingredients.slice())
    }
    deleteIngredients(index: number) {
        this.ingredients.splice(index,1);
         this.ingredientChanged.next(this.ingredients.slice())
    }
}