import { NgForm } from "@angular/forms";
import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Ingredient } from "../../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list.service";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  // thse are commented bcoz of using angular forms, earlierusing local ref we 
  // used to fetch the control info
  // @ViewChild('nameInput') nameInputRef: ElementRef;
  // @ViewChild('amountInput') amountInputRef: ElementRef;

  //This commented bcoz of using services
  // @Output() ingredientAdded = new EventEmitter<Ingredient>();

  constructor(private slService: ShoppingListService) { }
  private subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingredient;
  @ViewChild('f') slForm: NgForm;
  ngOnInit() {
    this.subscription = this.slService.startEditing.subscribe(
      (index: number) => {
        this.editMode = true;
        this.editedItemIndex = index;
        this.editedItem = this.slService.getIngrefient(index);
        this.slForm.setValue({
          name: this.editedItem.name,
          amount: this.editedItem.amount
        });

      }
    );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  OnClear() {
    this.slForm.reset();
    this.editMode = false;
  }
  OnDelete() {
    this.slService.deleteIngredients(this.editedItemIndex);
    this.OnClear();
  }
  onAddItem(form: NgForm) {
    // this used to get the local ref info usinf eleref;no longer needed as we are using now angular form
    //this.nameInputRef.nativeElement.value, this.amountInputRef.nativeElement.value
    const value = form.value;
    const newIngredient = new Ingredient(value.name, value.amount);
    if (this.editMode) {
      this.slService.updateIngredient(this.editedItemIndex, newIngredient);
    } else {
      // this.ingredientAdded.emit(newIngredient);
      this.slService.addIngredient(newIngredient);
    }
    this.editMode = false;
    form.reset();
  }

}
